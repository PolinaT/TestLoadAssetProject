// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Engine.h"
#include "Core.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "FCCARItem.h"
#include "CCDataTable.generated.h"

ENGINE_API DECLARE_LOG_CATEGORY_EXTERN(LogCCDataTable, Log, All);

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class TESTPROJECT_API UCCDataTable : public UObject
{
    GENERATED_BODY()

public:
	UCCDataTable();
    
    // CREATE
    UFUNCTION(BlueprintCallable, Category = "CCDataTable")
    bool AddNewRow(UDataTable *DataTable, FCCARItem ARItem) const;
    
    // READ
    UFUNCTION(BlueprintCallable, Category = "CCDataTable")
    bool GetRow(UDataTable* DataTable, FName ARItemName) const;
    
    // Update
    UFUNCTION(BlueprintCallable, Category = "CCDataTable")
    bool UpdateRow(UDataTable* DataTable, FCCARItem ARItem) const;
    
    //DELETE
    UFUNCTION(BlueprintCallable, Category = "CCDataTable")
    bool DeleteRow(UDataTable* DataTable, FName ARItemName) const;
};

