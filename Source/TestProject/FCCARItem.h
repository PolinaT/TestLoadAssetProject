// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "Engine.h"
#include "Core.h"
#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FCCARItem.generated.h"

/**
 *
 */
USTRUCT(BlueprintType, Blueprintable)
struct FCCARItem : public FTableRowBase
{
    GENERATED_BODY()
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Custom Item")
    FString Name;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Custom Item")
    FString DisplayedName;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Custom Item")
    UStaticMesh* Mesh;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Custom Item")
    FVector MaxAllowedBound;
};
