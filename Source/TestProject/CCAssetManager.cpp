// Fill out your copyright notice in the Description page of Project Settings.

#include "CCAssetManager.h"

DEFINE_LOG_CATEGORY(LogCCAssetManager);

UCCAssetManager::UCCAssetManager()
{
    UE_LOG(LogCCAssetManager, Warning, TEXT("CALLED CCAssetManager"));
}

bool UCCAssetManager::saveToFile(const TArray<uint8> &Data, const FString& Bucket,const FString& FileName, const FString& FileExtension) const
{
    if (Data.Num() <= 0) {
        return false;
    }

    const FString& Path = FPaths::ProjectContentDir() / "HandheldARBP/Models" / *Bucket / *FileName + "." + *FileExtension;
    
    if (FFileHelper::SaveArrayToFile(Data, *Path))
    {
        return true;
    }
    UE_LOG(LogCCAssetManager, Error, TEXT("Data Could not be saved in file %s!"), *Path);
    return false;

}

UStaticMesh * UCCAssetManager::LoadAssetFromFile(const FString& Bucket, const FString& FileName, const FString& MeshName) const
{
    const FString& Path = TEXT("/Game/HandheldARBP/Models/") + Bucket + TEXT("/") + FileName + "." + MeshName;
    return LoadObject<UStaticMesh>(NULL, *Path, NULL, LOAD_None, NULL);
    
    //return NULL;//Cast<UStaticMesh>object;
}


